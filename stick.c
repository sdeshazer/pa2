// Samantha DeShazer CSE 224 Fall 2019 Programming
// Programming Assignment 2
// ---------------------------------------------------------------------
// Purpose: the user uses the command stick to execute the console game,
// the user may input a second console command to input the number of
// initial sticks, if input is invalid, the program prompts error and
// exits. If no command input is given, the program will then prompt the
// user for input, if input is invalid, an error is displays and the
// program exits. Intial input must be atleast 10, if valid input is given,
// the game begins and the user gets the first turn in picking sticks to
// take. User must enter an integer between 1 and 3, if not the program
// will re-prompt for input until valid. The user takes turns with the
// computer, which ever player takes the last stick wins the game,
// the program then exits.
// --------------------------------------------------------------------


#include <stdio.h>
#include <stdlib.h>

// user Input Flags:
static enum Flag {
                  few_sticks, //err: too <10 sticks
                  not_int,    //err: not type int
                  no_cmd,     //more than 2 cmd arg
                  } flag;

// ------------------------------

// prototypes:
  void input(char**);
  void handleFlags();
  void gameBanner();
  void startBanner();
  void checkResult(int, int);
  void generateSticks(int);
  void prompt();
  void takeTurn(int);
  int checkTake(int, int);


// -------------------------------
int main(int argc, char* argv[]){

  if(argc==2){

    input(argv);

  }else{

    flag=no_cmd;
    handleFlags();

  }// else

  return 0;

}// main


// -----------------------
void input(char* argv[]){

  int sticks;
  char extra[100]; // stores characters
  int result=sscanf(argv[1],"%d%s",&sticks,extra);
  checkResult(result, sticks);

}// input


// ---------------------------------------
void checkResult(int result, int sticks){
  if(result==1){
    if(sticks >= 10){

      gameBanner();
      startBanner();
      takeTurn(sticks);

    }else{

     flag=few_sticks;
     handleFlags();

    }// else
  }else{

    // error flag:
    flag=not_int;
    handleFlags();

  }// else
}// checkResult


// -----------------
void handleFlags(){
   // handle messages for
   // different initial input errors:
   switch(flag){
     case few_sticks:
       printf("too few of sticks\n");
       break;
     case not_int:
       printf("not an integer (must also be whole-number)\n");
       break;
     case no_cmd:
       gameBanner();
       prompt();
       break;
     default:
       printf("Err.");

   }// switch
}// handleFlags


// ------------
void prompt(){

  printf("please enter the number of initial sticks ( 10 or more):\n");
  char temp[100];
  char extra[110];
  int sticks;

  fgets(temp,100,stdin);
  int result=sscanf(temp,"%d%s",&sticks,extra);
  checkResult(result, sticks);

}// prompt


// ----------------
void gameBanner(){

  printf("\n");
  printf("-*-*- S T I C K  G A M E -*-*- \n");

}// gameBanner


// -----------------
void startBanner(){

  printf("-*-*- G A M E  S T A R T -*-*- \n\n");

}// startBanner


// ------------------------------
void generateSticks(int sticks){

  // Generates |||| sticks:

  int i=0;
  printf("\n");
  printf("[Remaining Sticks:]\n");

  do{

    printf("|");
    i++;

  }while(i < sticks);
  printf("( %d )\n\n", sticks);

}// generateSticks


// ------------------------
void takeTurn(int sticks){

  int turn=0;
  char temp[100];
  char extra[100];
  int take;

  do{// while sticks < 0
    if(turn==0){ // turn 0 = user turn
      do{ // while turn 0

        generateSticks(sticks);
        printf("**USER'S TURN:\n");
        printf("How many sticks would you like to take (1,2 or 3) ?\n");
        fgets(temp,100,stdin);
        int result=sscanf(temp,"%d%s",&take,extra);

        // check take returns 0 if input is invalid:
        take=checkTake(result,take);
        sticks=sticks-take;

        if(take!=0){ // if input is valid:
          printf("USER took %d stick(s)\n\n",take);
        }// if

        if(sticks==0){ // if sticks run out:
          printf("-*-*-* U S E R  W I N S ! *-*-*-\n");
          exit(0);
         }// if

      }while(take==0);

      take=0;
      turn=1; // end user's turn

    }// if
    // ------------------------------
    if(turn==1){ // turn 1 = cpu turn

      generateSticks(sticks);
      printf("**CPU TURN:\n");
      take=(sticks%4);

      if (take==0){
        take=1;
      }// if

      sticks=sticks-take;
      printf("CPU took %d stick(s)\n\n", take);

      if(sticks==0){ // if sticks run out:
        printf("-*-*-* C P U  W I N S ! *-*-*-\n");
        exit(0);
      }// if

      turn=0;// end cpu's turn

    }// if
  }while(sticks > 0);
}// takeTurn


// ----------------------------------
int checkTake(int result, int take){

  if(result==1){
    if((take < 1)||(take > 3)){

      printf("*INV* Please enter an integer between 1 and 3\n");
      return 0;
    }else{

      return take;
    }// else
  }else{

    printf("*INV* please input an integer (must be whole number)\n");
    return 0;
  }// else
}// checkTake
